# Twitter bots
> Infinitely scalable Twitter bots. 'cause, why not?

Twitter bot orchestration using the [Serverless Framework](https://serverless.com)

## Installing / Getting started

After configuring your cloud services provider:
```shell
npm install
```

And, in each of your bots: 

```shell
npm install && serverless deploy
```

The first `npm install` sets up the testing environment. The subsequent shell commands install each bots dependancies and deploys the bot to your cloud provider of choice (AWS in this case) 

### Built With
[Serverless ^1.14](https://serverless.com)

### Prerequisites
Configure your cloud services provider per the [Serverless documentation](https://serverless.com/framework/docs)

## Tests

Test dependencies are kept at the root of the application while tests should be beside the modules that they test.
```shell
npm test
```

## Licensing

MIT