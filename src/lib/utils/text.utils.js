'use strict'

module.exports.getSwearWords = (string) => {
  return string.split(' ').filter((part) => module.exports.hasSwearWords(part))
}

module.exports.hasSwearWords = (string) => {
  let swearjar = require('swearjar')
  return swearjar.profane(string)
}

module.exports.stripSentenceTerminators = (sentence) => {
  if (sentence.lastIndexOf('.') == sentence.length - 1) 
    return sentence.slice(0, -1)
  return sentence
}

module.exports.stripLinks = (text) => {
  return text.replace(/(ht)?(f)?tps?(:)?\/\/\S+/g, '')
}

module.exports.stripTwitterHashTags = (tweet, fullreplace) => {
  if (!fullreplace) return tweet.replace(/#/g, '') 
  return tweet.replace(/#[a-zA-Z('s)?]+/g, '')
}
 
module.exports.stripTwitterMentions = (tweet) => {
  return tweet.replace(/(@[a-zA-Z\d]+(:?))| (@[a-zA-Z\d]+(\'s))| (@[a-zA-Z\d]+(\\'s))/g, '')
}

module.exports.stripTwitterRetweets = (tweet) => {
  return tweet.replace(/(^RT\s|^rt\s)|(\srt\s|\sRT\s)/g, ' ')
}

module.exports.stripEmoji = (string) => {
  // ugly but only want to require when needed
  return require('emoji-strip')(string)
}

module.exports.stripUndesiredChars = (string, chars) => {
  let parts = string.split(' ')
  let partslen = parts.length
  let result = []
  let regex

  for (var i = 0; i < partslen; i++) {
    if (chars.indexOf(parts[i]) >= 0) continue
    let word = parts[i]
    for (var j = 0; j < chars.length; j++) {
      regex = new RegExp(chars[j], 'g')
      word = word.replace(regex, '')
    }
    result.push(word)
  }
  return result.join(' ')
}

module.exports.condenseWhiteSpace = (string) => {
  // during parsing we run into scenarions where we have two
  // spaces inbetween a set or words.
  let result = []
  let parts = string.split(' ')
  
  for (let part of parts) {
    if (part.replace(/ /g, '')) result.push(part)
  }
  // coerce back into an array
  result = result.join(' ')
  // TODO: fix edge case to handle 'word , word'
  //       this can happen when we remove a mention
  //       where `hey @user, words` becomes `hey , words`
  return result.replace(/ ,/g, ',')
}
