'use strict'

let Rx = require('rxjs')
let Twitter = require('twitter')

let creds = {
  consumer_key:        process.env['CONSUMER_KEY'],
  consumer_secret:     process.env['CONSUMER_SECRET'],
  access_token_key:    process.env['ACCESS_TOKEN_KEY'],
  access_token_secret: process.env['ACCESS_TOKEN_SECRET']
}

class Twit {
  constructor () {
    this.client = new Twitter(creds)
  }

  updateStatus (text) {
    let params = { status: text }
    return Rx.Observable.fromPromise(this.client.post('statuses/update', params))
  }

  getStatuses (endpoint, count) {
    let params = { count: count }
    return Rx.Observable.fromPromise(this.client.get(`statuses/${endpoint}`, params))
  }

  getHomeTimeLine (count) {
    return this.getStatuses('home_timeline', count)
  }

  postImageStatus (image, status, cb) {
    return this.client.post('media/upload', { media: image }, (error, media, response) => {
      if (error) throw error
      const params = {
        status: status,
        media_ids: media.media_id_string
      }
      return this.client.post('statuses/update', params, cb)
    })
  }
}

module.exports = Twit
