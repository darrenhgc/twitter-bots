'use strict'

let expect = require('chai').expect
let sinon = require('sinon')
let proxyquire = require('proxyquire')

let getStub = sinon.stub().callsArg(1)
let putStub = sinon.stub().callsArg(1)
let listStub = sinon.stub().callsArg(1)
let deleteStub = sinon.stub().callsArg(1)
let AWS = require('aws-sdk')
sinon.stub(AWS, 'S3').callsFake(() => {
  return {
    getObject: getStub,
    putObject: putStub,
    listObjectsV2: listStub,
    deleteObject: deleteStub
  }
})

let bucketUtils = proxyquire('./bucket.utils.js', {
  'aws-sdk': AWS
})

let bucketName = 'bucketName'
let fileName = 'fileName'
let expected = {
  Bucket: bucketName,
  Key: fileName
}

describe('Bucket Utils', () => {
  beforeEach(() => {
    expected = {
      Bucket: bucketName,
      Key: fileName
    }
  })
  it('should get an item from a bucket', (done) => {
    bucketUtils.get(bucketName, fileName, () => {
      expect(getStub.callCount).to.equal(1)
      expect(getStub.args[0][0]).to.deep.equal(expected)
      done()
    })
  })

  it('should put an item into a bucket', (done) => {
    let data = 'some data'
    expected.Body = data
    bucketUtils.put(bucketName, fileName, data, () => {
      expect(putStub.callCount).to.equal(1)
      expect(putStub.args[0][0]).to.deep.equal(expected)
      done()
    })
  })

  it('should list the items in a bucket', (done) => {
    delete expected.Body
    delete expected.Key
    expected.MaxKeys = 10
    bucketUtils.listObjects(bucketName, () => {
      expect(listStub.callCount).to.equal(1)
      expect(listStub.args[0][0]).to.deep.equal(expected)
      done()
    })
  })

  it('should delete an item from a bucket', (done) => {
    bucketUtils.deleteObject(bucketName, fileName, () => {
      expect(deleteStub.callCount).to.equal(1)
      expect(deleteStub.args[0][0]).to.deep.equal(expected)
      done()
    })
  })
})