'use strict'

var expect = require('chai').expect
var proxyquire = require('proxyquire')
var sinon = require('sinon')

var Twitter = require('twitter')
var TwitterClient
var twitterRequestStub

describe('Twitter Client', () => {
  beforeEach(() => {
    if (twitterRequestStub) twitterRequestStub.restore()

    twitterRequestStub = sinon.stub(Twitter.prototype, '__request')
    .callsFake((method, path, params, cb) => {
      return Promise.resolve()
    })
    
    TwitterClient = proxyquire('./twitter.client.js', {
      'twitter': Twitter
    })
  })

  it('should update a status', (done) => {
    let actual = 'test'
    let expected = { status: actual }
    let client = new TwitterClient()
    client.updateStatus(actual)
    .catch(console.error)
    .subscribe(() => {
      let receivedArgs = twitterRequestStub.args[0]
      expect(twitterRequestStub.calledOnce).to.be.true
      expect(receivedArgs[0]).to.equal('post')
      expect(receivedArgs[1]).to.equal('statuses/update')
      expect(receivedArgs[2]).to.deep.equal(expected)
      done()
    })
  })

  it('should send media files', (done) => {
    let client = new TwitterClient()
    client.postImageStatus({}, 'status', (err, resp) => {
      expect(twitterRequestSTub.calledOnce).to.be.true
    })
    .then(done, done)
  })

  it('should get the home timeline', (done) => {
    let actual = 10
    let expected = { count: actual }
    let client = new TwitterClient()
    client.getHomeTimeLine(actual)
    .catch(console.error)
    .subscribe(() => {
      let receivedArgs = twitterRequestStub.args[0]
      expect(twitterRequestStub.calledOnce).to.be.true
      expect(receivedArgs[0]).to.equal('get')
      expect(receivedArgs[1]).to.equal('statuses/home_timeline')
      expect(receivedArgs[2]).to.deep.equal(expected)
      done()
    })
  })
})
