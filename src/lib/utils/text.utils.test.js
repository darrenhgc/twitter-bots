'use strict'
// test setup
var chai = require('chai')
var expect = chai.expect

// test expectations
var utils = require('./text.utils.js')
var tweet = 'hey @twitterUser:, what\'s shaking? RT @otherUser\'s SPORT #reporter'
var expected, actual

describe ('Text utils', () => {
  it ('should tell me if text is vulgar', () => {
    expect(utils.hasSwearWords('fuck you bitch')).to.be.true
    expect(utils.hasSwearWords('you are amazing')).to.be.false
  })

  it ('should remove periods from the end of sentences', () => {
    expect(utils.stripSentenceTerminators('howdy y\'all.')).to.equal('howdy y\'all')
  })

  it ('should return sentence when no terminator found', () => {
    expect(utils.stripSentenceTerminators('howdy y\'all')).to.equal('howdy y\'all')
  })

  it ('should give me each swear word', () => {
    expect(utils.getSwearWords('fuck you bitch')).to.deep.equal(['fuck', 'bitch'])
  })

  it ('should remove https links', () => {
    actual = 'visit https://somelink.com now'
    expected = 'visit  now'
    expect(utils.stripLinks(actual)).to.equal(expected)
  })

  it ('should remove http links', () => {
    actual = 'visit http://somelink.com now'
    expected = 'visit  now'
    expect(utils.stripLinks(actual)).to.equal(expected)

    actual = 'jim donovan withdraws as nominee for u.s. treasury deputy secretary https://t.co/eohpln3csr'
    expected = 'jim donovan withdraws as nominee for u.s. treasury deputy secretary '
    expect(utils.stripLinks(actual)).to.equal(expected)
  })

  it ('should remove ftp links', () => {
    actual = 'visit ftp://somelink.com now'
    expected = 'visit  now'
    expect(utils.stripLinks(actual)).to.equal(expected)
  })

  it ('should remove shortened links', () => {
    actual = 'visit https//t.co/TINYURL now'
    expected = 'visit  now'
    expect(utils.stripLinks(actual)).to.equal(expected)
  })

  it ('should strip twitter mentions', () => {
    expected = 'hey , what\'s shaking? RT SPORT #reporter'
    expect(utils.stripTwitterMentions(tweet)).to.equal(expected)
  })
  
  it ('should strip twitter mentions that contain numbers', () => {
    actual = 'watch @ABC7kevin\'s stuff'
    expected = 'watch stuff'
    expect(utils.stripTwitterMentions(actual)).to.equal(expected)
  })

  it ('should strip (RT|rt) from tweets', () => {
    expected = 'hey @twitterUser:, what\'s shaking? @otherUser\'s SPORT #reporter'
    expect(utils.stripTwitterRetweets(tweet)).to.equal(expected)
    
    actual = 'RT @tweeter: great going!'
    expect(utils.stripTwitterRetweets(actual)).to.equal(' @tweeter: great going!')
  })

  it ('should strip hashtags chars', () => {
    expected = 'hey @twitterUser:, what\'s shaking? RT @otherUser\'s SPORT reporter'
    expect(utils.stripTwitterHashTags(tweet)).to.equal(expected)
  })

  it ('should strip entire hashtag', () => {
    expected = 'hey @twitterUser:, what\'s shaking? RT @otherUser\'s SPORT '
    expect(utils.stripTwitterHashTags(tweet, true)).to.equal(expected)
  })

  it ('should replace given chars', () => {
    actual = 'Wha.t - have. we. here? yall...'
    expected = 'What have we here yall'
    let undesired = ['-', '\\?', '\\...', '\\*', '\\.']
    expect(utils.stripUndesiredChars(actual, undesired)).to.equal(expected)
  })

  it ('should squeeze string together', () => {
    actual = '  hey  there  user  ,  how  are  you  '
    expected = 'hey there user, how are you'
    expect(utils.condenseWhiteSpace(actual)).to.equal(expected)
  })

  it ('should strip emoji chars from string', () => {
    actual = ' lanes of I-85 to open! 🚗 Be the FIRST'
    expected = ' lanes of I-85 to open!  Be the FIRST'
    expect(utils.stripEmoji(actual)).to.equal(expected)
  })
})
