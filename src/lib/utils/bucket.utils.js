'use strict'

const AWS = require('aws-sdk')
const s3 = new AWS.S3()

module.exports.get = (bucketName, fileName, cb) => {
  const params = { Bucket: bucketName, Key: fileName }
  return s3.getObject(params, cb)
}

module.exports.put = (bucketName, fileName, data, cb) => {
  return s3.putObject({
    Bucket: bucketName,
    Key: fileName,
    Body: data
  }, cb)
}

module.exports.listObjects = (bucketName, cb) => {
  const params = { Bucket: bucketName, MaxKeys: 10 }
  return s3.listObjectsV2(params, cb)
}

module.exports.deleteObject = (bucketName, fileName, cb) => {
  const params = { Bucket: bucketName, Key: fileName }
  return s3.deleteObject(params, cb)
}
