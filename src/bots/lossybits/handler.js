'use strict'

const Twitter = require('lib/utils/twitter.client.js')
const bucketUtils = require('lib/utils/bucket.utils.js')
const Jimp = require('jimp')
const async = require('async')
const fs = require('fs')

let randomImage

const listImages = (next) => {
  console.time('list images')
  bucketUtils.listObjects(process.env.INBUCKET, next)
  console.timeEnd('list images')
}

const downloadImage = (response, next) => {
  console.time('get image')
  const imageList = response.Contents.map((item) => { return item.Key })
  randomImage = imageList[Math.floor(Math.random() * imageList.length)]
  bucketUtils.get(process.env.INBUCKET, randomImage, next)
  console.timeEnd('get image')
}

const cleanImageBucket = (response, next) => {
  console.time('delete image')
  bucketUtils.deleteObject(process.env.INBUCKET, randomImage, next)
  console.timeEnd('delete image')
}

const persistImage = (response, next) => {
  console.time('persist image')
  fs.writeFile('/tmp/image.jpeg', response.Body, (error, data) => {
    if (error) throw error
    return next(null, response.Key)
  })
  console.timeEnd('persist image')
}

const toJPEG = (response, next) => {
  console.time('convert to jpeg')
  Jimp.read('/tmp/image.png')
  .then((image) => {
    image.resize(Jimp.AUTO, 180)
    .quality(1)
    .write('/tmp/image.jpeg', () => next(null, {}))
  })
  console.timeEnd('convert to jpeg')
}

const toPNG = (response, next) => {
  console.time('convert to png')
  Jimp.read('/tmp/image.jpeg')
  .then((image) => {
    image.resize(Jimp.AUTO, 180)
    .quality(1)
    .write('/tmp/image.png', () => next(null, {}))
  })
  console.timeEnd('convert to png')
}

const finalizeImage = (response, next) => {
  console.time('finalize image')
  Jimp.read('/tmp/image.jpeg')
  .then((image) => {
    image.resize(Jimp.AUTO, 180)
    .quality(1)
    .write('/tmp/lossybit.png', () => next(null, {}))
  })
  console.timeEnd('finalize image')
}

const updateStatus = (response, next) => {
  console.time('update status')
  let image = fs.readFileSync('/tmp/lossybit.png')
  let twitter = new Twitter()
  twitter.postImageStatus(image, '', next)
  console.timeEnd('update status')
}

const tasks = [ listImages, downloadImage, persistImage, cleanImageBucket, toPNG, toJPEG, finalizeImage, updateStatus]
exports.lossybits = (event, context, callback ) => {
  async.waterfall(tasks, (error, result) => {
    if (error) console.error(JSON.stringify(error))
  })
}
