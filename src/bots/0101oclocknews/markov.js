'use strict'

class Markov {
  constructor () {
    this.terminals = {}
    this.startwords = []
    this.wordstats = {}
  }

  train (dataset) {
    if (typeof dataset !== typeof []) throw new Error('dataset should be an array')
    if (dataset.lengt < 1) throw new Error('data set can not be empty')

    var limit = dataset.length
    for (var i = 0; i < limit; i++) {
      var words = dataset[i].split(' ')
      this.terminals[words[words.length - 1]] = true
      this.startwords.push(words[0])

      for (var j = 0; j < words.length -1; j++) {
        if (this.wordstats.hasOwnProperty(words[j])) {
          this.wordstats[words[j]].push(words[j + 1])
        } else {
          this.wordstats[words[j]] = [words[j + 1]]
        }
      }
    }
    return this
  }

  make (min) {
    let choice = (a) => {
      var i = Math.floor(a.length * Math.random())
      return a[i]
    }

    var word = choice(this.startwords)
    var result = [word]
    while (this.wordstats.hasOwnProperty(word)) {
      var nextwords = this.wordstats[word]
      word = choice(nextwords)
      result.push(word)
      if (result.length > min && this.terminals.hasOwnProperty(word)) break
    }
    return result.join(' ')
  }
}

module.exports = Markov
