var expect = require('chai').expect
var proxyquire = require('proxyquire')
var sinon = require('sinon')

let Markov = require('./markov')

describe("Markov", () => {
  it("should exist", () => {
    let markov = new Markov()
    expect(markov).not.to.equal(undefined)
  })
})
