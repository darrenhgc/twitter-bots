'user strict'

var Feed = require('rss-to-json')
var async = require('async')
var Markov = require('./markov.js')
var Twitter = require('./lib/utils/twitter.client.js')
var newsfeeds = require('./newsfeeds.js')

function randomNumber(min, max) {
  return Math.floor(
  Math.random() * (max - min) + min)
}

handleFeedResponse = (error, results) => {
  if (error) throw error
  var headlines = []
  for (var source in results) {
    if (!results.hasOwnProperty(source)) continue
    let articles = results[source].items
    articles.forEach(article => {
      var headline = article.title
        .replace('<![CDATA[', '')
        .replace(']]>', '')
        .replace('&apos;', '\'')
      
      if (headline.length > 1) headlines.push(headline)
    })
  }
  generateMarkovHeadline(headlines)
}

generateMarkovHeadline = (titles) => {
  var headlines = new Markov().train(titles).make(randomNumber(6, 12))
  if (headlines.length > 130) return // tweet would be too big
  var twitter = new Twitter()
  twitter.updateStatus(headlines)
}

loadFeed = (source, key, callback) => {
  Feed.load(source, callback)
}

makeSportsNews = () => {
  async.mapValues(newsfeeds.sports, loadFeed, handleFeedResponse)
}

makeHeadlineNews = () => {
  async.mapValues(newsfeeds.breaking, loadFeed, handleFeedResponse)
}

broadcastNews = () => {
  makeSportsNews()
  makeHeadlineNews()
}

exports.ohOneOhOneOclockNewsBot = (event, context, callback) => broadcastNews()
