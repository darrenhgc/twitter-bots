'use strict'
let Twitter = require('./lib/utils/twitter.client.js')
let stripEmojis = require('./lib/utils/text.utils.js').stripEmoji
let hasSwearWords = require('./lib/utils/text.utils.js').hasSwearWords
let stripHashTags = require('./lib/utils/text.utils.js').stripTwitterHashTags
let stripLinks = require('./lib/utils/text.utils.js').stripLinks
let stripRetweets = require('./lib/utils/text.utils.js').stripTwitterRetweets
let stripMentions = require('./lib/utils/text.utils.js').stripTwitterMentions
let stripUndesiredChars = require('./lib/utils/text.utils.js').stripUndesiredChars
let consenseWhiteSpace = require('./lib/utils/text.utils.js').condenseWhiteSpace
let stripSentenceTerminators = require('./lib/utils/text.utils.js').stripSentenceTerminators
// natural language stuff
let Syllable = require('syllable')
let Tokenizer = require('sentence-tokenizer')
let tokenizer = new Tokenizer('Alice')
let undesiredChars = [':', '"', '\\?', '\_', ':', '\\*', '!']

// count syllables
let syllabize = (document) => {
  tokenizer.setEntry(document)
  let sentences = tokenizer.getSentences()
  let results = []
  return sentences.map((sentence) => {
    return {
      count: Syllable(sentence),
      text: stripSentenceTerminators(sentence)
    }
  })
}

let generateHaiku = (fives, sevens) => {
  let index, lineOne, lineTwo, lineThree
  index = Math.floor(Math.random() * fives.length)
  lineOne = fives.splice(index, 1)[0]
  
  lineTwo = sevens[Math.floor(Math.random() * sevens.length)]
  
  index = Math.floor(Math.random() * fives.length)
  lineThree = fives[index]
  
  return `${lineOne.text}\n${lineTwo.text}\n${lineThree.text}\n#haiku`
}

let handleSuccess = (tweets) => {
  let fives = []
  let sevens = []
  let text = ''
  tweets.forEach((tweet) => {
    let results = syllabize(tweet)
    for (let result of results) {
      if (result.count == 5) fives.push(result)
      if (result.count == 7) sevens.push(result)
    }
  })
  
  if (fives.length >= 2 && sevens.length >= 1) {
    
    let status = generateHaiku(fives, sevens)
    
    console.log('updating status..')
    console.log(status)
    let twitter = new Twitter()
    twitter.updateStatus(status)
  } else {
    console.log('nothing doing')
    console.log(fives)
    console.log(sevens)
  }
}

let haikuBot = (count) => {
  let twitter = new Twitter()
  twitter.getHomeTimeLine(count)
  .map(tweets => tweets.filter(tweet => tweet.user.screen_name !== '575on'))
  .map(tweets => tweets.map(tweet => tweet.text.toLowerCase()))
  .map(tweets => tweets.map(stripMentions))
  .map(tweets => tweets.map(stripRetweets))
  .map(tweets => tweets.map(stripHashTags))
  .map(tweets => tweets.map(stripEmojis))
  .map(tweets => tweets.map(stripLinks))
  .map((tweets) => tweets.map((tweet) => stripUndesiredChars(tweet, undesiredChars)))
  .map(tweets => tweets.map(consenseWhiteSpace)) 
  .catch(errors => console.error(JSON.stringify(errors)))
  .subscribe(handleSuccess)
}

exports.haikubot = (event, context, callback ) => haikuBot(450)
